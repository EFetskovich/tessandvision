package project.tess.itransition.myapplication

import org.junit.Test
import project.tess.itransition.myapplication.utils.tesseract.TextStabilizer

/**
 * @author e.fetskovich on 10/22/18.
 */
class TextStabilizerTest {

    private val TEXT_TO_STABILIZE_CORRECT = "1A2d-"

    private val TEXT_TO_STABILIZE_TWO = "1A2d-*&%\na+_%2)$@"
    private val TEXT_TO_STABILIZE_TWO_RESULT = "1A2d-\na2"

    private val TEXT_TO_STABILIZE_THREE = "1A2d-*&%\na+_%*0@#$)$@"
    private val TEXT_TO_STABILIZE_THREE_RESULT = "1A2d-\na0"

    @Test
    fun checkTextStabilizationOne() {
        checkStabilization(TEXT_TO_STABILIZE_CORRECT, TEXT_TO_STABILIZE_CORRECT)
    }

    @Test
    fun checkTextStabilizationTwo() {
        checkStabilization(TEXT_TO_STABILIZE_TWO, TEXT_TO_STABILIZE_TWO_RESULT)
    }

    @Test
    fun checkTextStabilizationThree() {
        checkStabilization(TEXT_TO_STABILIZE_THREE, TEXT_TO_STABILIZE_THREE_RESULT)
    }

    private fun checkStabilization(textToProceed: String, expectedResult: String) {
        val text = TextStabilizer.stabilizeAlphanumericText(textToProceed)
        System.out.println(text)
        assert(text.equals(expectedResult))
    }

}