package project.tess.itransition.myapplication.utils.scanner

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.StrictMode
import android.provider.MediaStore
import project.tess.itransition.myapplication.utils.FileUtils
import project.tess.itransition.myapplication.utils.scanner.strategy.process_image.FilepathProcessImageStrategy
import project.tess.itransition.myapplication.utils.scanner.strategy.process_image.ProcessImageStrategy
import project.tess.itransition.myapplication.utils.scanner.strategy.process_image.UriProcessImageStrategy
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.ImageScanStrategy
import java.io.File
import java.util.*


/**
 * @author e.fetskovich on 10/19/18.
 */
class ImageScanner(
    protected var activity: Activity,
    var imageScanStrategy: ImageScanStrategy,
    private val directoryPathOriginal: String,
    var scannerListener: ImageScannerListener? = null
) {

    private var filePathOriginal: String? = null

    fun capturePicture(requestCode: Int) {
        // Changing api policy for use Uri for camera, temp fast solution
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        this.filePathOriginal = FileUtils.getDirectory(this.directoryPathOriginal) + File.separator +
                Calendar.getInstance().timeInMillis + IMAGE_FORMAT
        intent.putExtra("output", Uri.fromFile(File(this.filePathOriginal)))

        startActivity(intent, requestCode)
    }

    fun takePhoto(requestCode: Int) {
        val intent = Intent(Intent.ACTION_PICK)
        intent.setType("image/*")
        startActivity(intent, requestCode)
    }

    fun onImagePicked(uri: Uri) {
        startImageProcessor(
            UriProcessImageStrategy(
                uri,
                activity
            )
        )
    }

    fun onImageTaken() {
        if (filePathOriginal != null) {
            startImageProcessor(
                FilepathProcessImageStrategy(
                    filePathOriginal!!,
                    activity
                )
            )
        }
    }

    private fun startImageProcessor(processImageStrategy: ProcessImageStrategy) {
        if (scannerListener != null) {
            ScannerImageProcessor(
                scannerListener!!,
                processImageStrategy,
                imageScanStrategy
            ).execute()
        }
    }

    private fun startActivity(intent: Intent, requestCode: Int) {
        this.activity.startActivityForResult(intent, requestCode)
    }

    companion object {
        private val IMAGE_FORMAT = ".jpg"
    }

}
