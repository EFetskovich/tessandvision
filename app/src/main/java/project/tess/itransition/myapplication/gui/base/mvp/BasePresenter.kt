package project.tess.itransition.myapplication.gui.base.mvp

/**
 * @author e.fetskovich on 10/19/18.
 */
interface BasePresenter<V : BaseView> {
    fun attach(view: V)
    fun detach()
}