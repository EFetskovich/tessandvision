package project.tess.itransition.myapplication.gui.scanner

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.annotation.IdRes
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import project.tess.itransition.myapplication.R
import project.tess.itransition.myapplication.gui.base.components.BaseActivity
import project.tess.itransition.myapplication.utils.FileUtils
import project.tess.itransition.myapplication.utils.scanner.ImageScanner
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.GmsImageScanStrategy
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.TessImageScanStrategy
import project.tess.itransition.myapplication.utils.tesseract.TessFilesInitializer

/**
 * @author e.fetskovich on 10/18/18.
 */
class MainActivity : BaseActivity(), MainActivityContract.ViewHelper {

    private var presenter: MainActivityContract.Presenter? = null
    private var mode: Int = R.id.tesseractMode

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initComponents()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detach()
    }

    private fun initComponents() {
        presenter = MainActivityPresenter(
            ImageScanner(this, GmsImageScanStrategy(this), DIRECTORY_PATH)
        )
        presenter?.attach(this)
        initTessFiles()

        btnCammera.setOnClickListener { captureImage() }
        btnGallery.setOnClickListener { pickImage() }

        tesseractMode.setOnClickListener(onRadioButtonClick)
        visionMode.setOnClickListener(onRadioButtonClick)
    }

    private fun initTessFiles() {
        TessFilesInitializer.initTessFiles(
            this,
            FileUtils.getDirectory(DIRECTORY_PATH),
            TESS_TRAINGED_LANG,
            TESS_TRAINED_NAME,
            null
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CAMERA_PERMISSION ->
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    captureImage()
                }
            REQUEST_READ_GALLERY_PERMISSION ->
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    pickImage()
                }
        }
    }

    private fun captureImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
            return
        }

        presenter?.captureImage(REQUEST_CODE_CAPTURE_IMAGE)

    }

    private fun pickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_READ_GALLERY_PERMISSION)
                return
            }
        }

        presenter?.pickImage(REQUEST_CODE_PICK_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_CAPTURE_IMAGE -> presenter?.onImageCaptured()
                REQUEST_CODE_PICK_IMAGE -> presenter?.onImagePicked(data?.data)
            }
        }
    }

    override fun showErrorMessage() {
        Toast.makeText(this@MainActivity, "Scan FAILED", Toast.LENGTH_SHORT).show()
    }

    override fun setLoadedData(bitmap: Bitmap?, text: String?) {
        tessResult?.text = text
        image?.setImageBitmap(bitmap)
    }

    override fun showLoadingDialog() {
        showProgressDialog()
    }

    override fun hideLoadingDialog() {
        hideProgressDialog()
    }

    private val onRadioButtonClick = object : View.OnClickListener {
        override fun onClick(view: View) {
            switchModeTo((view as RadioButton).id)
        }
    }

    private fun switchModeTo(@IdRes id: Int) {
        this.mode = id
        setLoadedData(null, "")
        when (id) {
            R.id.tesseractMode -> presenter?.changeImageScannerType(
                TessImageScanStrategy(
                    DIRECTORY_PATH,
                    TESS_TRAINGED_LANG
                )
            )
            R.id.visionMode -> presenter?.changeImageScannerType(GmsImageScanStrategy(this@MainActivity))
        }
    }


    companion object {
        val REQUEST_CAMERA_PERMISSION = 103
        val REQUEST_READ_GALLERY_PERMISSION = 104

        val REQUEST_CODE_CAPTURE_IMAGE = 2000
        val REQUEST_CODE_PICK_IMAGE = 2001

        val TESS_TRAINGED_LANG = "eng"
        val DIRECTORY_PATH = "TessScanner"
        val TESS_TRAINED_NAME = ".traineddata"
    }
}