package project.tess.itransition.myapplication

/**
 * @author e.fetskovich on 10/19/18.
 */
object TessConfig {
    val TAG = "TessCamera"
    val TESS_DATA_PATH_PREFIX = "tessdata"
}
