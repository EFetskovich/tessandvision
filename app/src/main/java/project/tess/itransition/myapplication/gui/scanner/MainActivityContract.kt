package project.tess.itransition.myapplication.gui.scanner

import android.graphics.Bitmap
import android.net.Uri
import project.tess.itransition.myapplication.gui.base.mvp.BasePresenter
import project.tess.itransition.myapplication.gui.base.mvp.BaseView
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.ImageScanStrategy

/**
 * @author e.fetskovich on 10/19/18.
 */
interface MainActivityContract {

    interface ViewHelper : BaseView {
        fun showErrorMessage()
        fun setLoadedData(bitmap: Bitmap?, text: String?)
        fun showLoadingDialog()
        fun hideLoadingDialog()
    }

    interface Presenter : BasePresenter<ViewHelper> {
        fun pickImage(requestCode: Int)
        fun captureImage(requestCode: Int)
        fun onImageCaptured()
        fun onImagePicked(uri: Uri?)

        fun changeImageScannerType(strategy: ImageScanStrategy)
    }

}