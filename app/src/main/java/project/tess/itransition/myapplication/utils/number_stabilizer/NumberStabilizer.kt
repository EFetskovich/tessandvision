package project.tess.itransition.myapplication.utils.number_stabilizer

/**
 * @author e.fetskovich on 10/22/18.
 */
object NumberStabilizer {

    var mapOfEqualsNumbers = mapOf(
        'B' to "3",
        'b' to "6",
        'i' to "1",
        'I' to "1"
    )

    fun changeTextByMapOfSameNumbers(text: String): String {
        var newValue = ""
        text.forEachIndexed { index, char ->
            if (mapOfEqualsNumbers.containsKey(char)) {
                newValue = newValue.plus(mapOfEqualsNumbers.get(char))
            } else {
                newValue = newValue.plus(char)
            }
        }
        return newValue
    }

}