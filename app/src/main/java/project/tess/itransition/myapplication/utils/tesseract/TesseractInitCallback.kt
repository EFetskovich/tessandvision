package project.tess.itransition.myapplication.utils.tesseract

/**
 * @author e.fetskovich on 10/19/18.
 */
interface TesseractInitCallback {
    fun onTesseractInitFinished()
    fun onTesseractInitFailed()
}
