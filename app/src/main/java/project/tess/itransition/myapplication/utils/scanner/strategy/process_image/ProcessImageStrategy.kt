package project.tess.itransition.myapplication.utils.scanner.strategy.process_image

import android.graphics.Bitmap

/**
 * @author e.fetskovich on 10/19/18.
 */
interface ProcessImageStrategy {
    fun processImage(): Bitmap?
}