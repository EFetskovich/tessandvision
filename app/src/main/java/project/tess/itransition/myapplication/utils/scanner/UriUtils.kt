package project.tess.itransition.myapplication.utils.scanner

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri


/**
 * @author e.fetskovich on 10/19/18.
 */
object UriUtils {

    fun convertUriToBitmap(uri: Uri, activity: Activity): Bitmap? {
        var bitmap: Bitmap? = null
        try {
            val stream = activity.contentResolver.openInputStream(uri)
            bitmap = BitmapFactory.decodeStream(stream)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return bitmap
    }

}