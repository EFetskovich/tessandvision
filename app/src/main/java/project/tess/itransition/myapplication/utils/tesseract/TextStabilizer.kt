package project.tess.itransition.myapplication.utils.tesseract

/**
 * @author e.fetskovich on 10/22/18.
 */
object TextStabilizer {

    const val ALPHA_NUMERICAL_REGEX = "[^A-Za-z0-9-\n]"

    fun stabilizeAlphanumericText(text: String): String {
        val regex = Regex(ALPHA_NUMERICAL_REGEX)
        return regex.replace(text, "")
    }

}