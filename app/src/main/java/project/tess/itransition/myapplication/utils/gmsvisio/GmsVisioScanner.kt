package project.tess.itransition.myapplication.utils.gmsvisio

import android.content.Context
import android.graphics.Bitmap
import android.util.SparseArray
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import project.tess.itransition.myapplication.utils.number_stabilizer.NumberStabilizer

/**
 * @author e.fetskovich on 10/22/18.
 */
object GmsVisioScanner {

    private val TEXT_SYMBOLS_INDEFIED = 3
    private val FIND_WORDS_BLOCK_REGEX = "([a-zA-Z]{$TEXT_SYMBOLS_INDEFIED})"

    fun scanImage(context: Context, bitmap: Bitmap, stabilize: Boolean = true): String? {
        try {

            val textRecog = TextRecognizer.Builder(context).build()
            val frame = Frame.Builder()
                .setBitmap(bitmap)
                .build()

            val textBlocks = textRecog.detect(frame)

            return initTextValue(textBlocks, stabilize)
        } catch (e: Exception) {
            return null
        }
    }

    private fun initTextValue(textBlocks: SparseArray<TextBlock>, stabilize: Boolean): String {
        var text = ""
        for (i in 0 until textBlocks.size()) {
            val value = textBlocks.get(textBlocks.keyAt(i)).value

            // If the input contains @TEXT_SYMBOLS_INDEFIED count words then do not add it to the result
            if (!value.contains(FIND_WORDS_BLOCK_REGEX.toRegex())) {
                if (stabilize) {
                    text = text.plus(NumberStabilizer.changeTextByMapOfSameNumbers(value))
                } else {
                    text = text.plus(value)
                }
            }

        }
        return text
    }

}