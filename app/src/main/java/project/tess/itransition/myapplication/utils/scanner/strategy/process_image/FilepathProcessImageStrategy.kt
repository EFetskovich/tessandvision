package project.tess.itransition.myapplication.utils.scanner.strategy.process_image

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import project.tess.itransition.myapplication.utils.ScreenUtils
import java.io.File

/**
 * @author e.fetskovich on 10/19/18.
 */
class FilepathProcessImageStrategy(val filePath: String, val activity: Activity) :
    ProcessImageStrategy {
    override fun processImage(): Bitmap? {
        val rawBitmap = getBitmapFromPath()
        if (rawBitmap != null) {
            return ScreenUtils.rotateBitmap(
                rawBitmap,
                ScreenUtils.getOrientation(activity, Uri.fromFile(File(filePath)))
            )
        } else {
            return null
        }
    }

    private fun getBitmapFromPath(): Bitmap? {
        val bmOptions = BitmapFactory.Options()
        bmOptions.inSampleSize = 4
        return BitmapFactory.decodeFile(filePath, bmOptions)
    }

}