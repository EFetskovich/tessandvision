package project.tess.itransition.myapplication.utils.tesseract

import android.app.Activity
import project.tess.itransition.myapplication.TessConfig
import project.tess.itransition.myapplication.utils.FileUtils
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * @author e.fetskovich on 10/19/18.
 */
object TessFilesInitializer {

    fun initTessFiles(
        activity: Activity, absoluteDirectoryPath: String,
        langName: String, langPrefix: String, callback: TesseractInitCallback?
    ) {
        val tessDirectoryPath = FileUtils.getTessdataDirectory(absoluteDirectoryPath)

        if (!File("$tessDirectoryPath/$langName$langPrefix").exists()) {
            try {
                val assetManager = activity.assets
                val mainPrefix = langName + langPrefix

                FileUtils.writeFile(
                    assetManager.open(TessConfig.TESS_DATA_PATH_PREFIX + "/" + mainPrefix),
                    FileOutputStream("$tessDirectoryPath/$mainPrefix")
                )

                callback?.onTesseractInitFinished()
            } catch (e: IOException) {
                callback?.onTesseractInitFailed()
            }

        } else {
            callback?.onTesseractInitFinished()
        }
    }

}
