package project.tess.itransition.myapplication.utils.scanner.strategy.process_image

import android.app.Activity
import android.graphics.Bitmap
import android.net.Uri
import project.tess.itransition.myapplication.utils.ScreenUtils
import project.tess.itransition.myapplication.utils.scanner.UriUtils


/**
 * @author e.fetskovich on 10/19/18.
 */
class UriProcessImageStrategy(var uri: Uri, var activity: Activity) :
    ProcessImageStrategy {
    override fun processImage(): Bitmap? {
        var bitmap = UriUtils.convertUriToBitmap(uri, activity)

        if (bitmap != null) {
            bitmap = ScreenUtils.rotateBitmap(
                bitmap,
                ScreenUtils.getOrientation(activity, uri)
            )
        }

        return bitmap
    }


}