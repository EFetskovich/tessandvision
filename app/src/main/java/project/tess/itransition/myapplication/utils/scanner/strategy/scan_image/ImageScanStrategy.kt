package project.tess.itransition.myapplication.utils.scanner.strategy.scan_image

import android.graphics.Bitmap

/**
 * @author e.fetskovich on 10/22/18.
 */
abstract class ImageScanStrategy {

    @Throws(Exception::class)
    abstract fun scanImage(bitmap: Bitmap): String?

    fun scanImageToReceiveItText(bitmap: Bitmap?): String? {
        if (bitmap == null) {
            return null
        }

        try {
            return scanImage(bitmap)
        } catch (e: Exception) {
            return null
        }

    }

}