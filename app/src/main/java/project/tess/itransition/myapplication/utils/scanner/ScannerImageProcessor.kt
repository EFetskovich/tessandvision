package project.tess.itransition.myapplication.utils.scanner

import android.graphics.Bitmap
import android.os.AsyncTask
import project.tess.itransition.myapplication.utils.scanner.strategy.process_image.ProcessImageStrategy
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.ImageScanStrategy

/**
 * @author e.fetskovich on 10/19/18.
 */
class ScannerImageProcessor(
    private val scannerListener: ImageScannerListener,
    private val processImageStrategy: ProcessImageStrategy,
    private val imageScanStrategy: ImageScanStrategy
) : AsyncTask<Void?, Void?, Void?>() {

    private var bitmap: Bitmap? = null
    private var scannedText: String? = null

    override fun onPreExecute() {
        super.onPreExecute()
        scannerListener.onScanStarted()
    }

    override fun doInBackground(vararg params: Void?): Void? {
        bitmap = processImageStrategy.processImage()
        scannedText = scanImage()
        return null
    }

    override fun onPostExecute(aVoid: Void?) {
        super.onPostExecute(aVoid)
        if (bitmap != null && scannedText != null) {
            scannerListener.onScanFinished(bitmap!!, scannedText!!)
        } else {
            scannerListener.onScanFailed()
        }
    }

    private fun scanImage(): String? {
        return imageScanStrategy.scanImageToReceiveItText(bitmap)
    }
}
