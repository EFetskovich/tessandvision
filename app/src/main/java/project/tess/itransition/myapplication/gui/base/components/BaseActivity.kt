package project.tess.itransition.myapplication.gui.base.components

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import project.tess.itransition.myapplication.utils.UiUtils

/**
 * @author e.fetskovich on 10/19/18.
 */
abstract class BaseActivity : AppCompatActivity() {
    protected var progressDialog: ProgressDialog? = null

    override fun onDestroy() {
        super.onDestroy()
        hideProgressDialog()
    }

    protected fun showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = UiUtils.getBaseProgressDialog(this, false)
            }

            if (progressDialog != null && !isFinishing) {
                if (progressDialog?.isShowing() == false) {
                    progressDialog?.show()
                }

            }
        } catch (e: Exception) {
            // Window decor manager exception
        }
    }

    protected fun hideProgressDialog() {
        if (progressDialog != null && progressDialog?.isShowing() == true && !isFinishing) {
            progressDialog?.dismiss()
        }
    }

}