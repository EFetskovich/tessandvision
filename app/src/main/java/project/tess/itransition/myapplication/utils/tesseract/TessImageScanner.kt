package project.tess.itransition.myapplication.utils.tesseract

import android.graphics.Bitmap
import com.googlecode.tesseract.android.TessBaseAPI
import project.tess.itransition.myapplication.utils.FileUtils

/**
 * @author e.fetskovich on 10/19/18.
 */
object TessImageScanner {

    fun scanImage(directionPath: String, trainedDataCode: String, bitmap: Bitmap, stabilize: Boolean = true): String? {
        try {
            var recognizedText = recognizeText(directionPath, trainedDataCode, bitmap)

            if (stabilize && recognizedText != null) {
                recognizedText = TextStabilizer.stabilizeAlphanumericText(recognizedText)
            }

            return recognizedText
        } catch (e: Exception) {
            return null
        }
    }

    @Throws(Exception::class)
    fun recognizeText(directionPath: String, trainedDataCode: String, bitmap: Bitmap): String? {
        val baseApi = TessBaseAPI()
        baseApi.init(FileUtils.getDirectory(directionPath) + "/", trainedDataCode)
        baseApi.setImage(bitmap)
        val recognizedText = baseApi.utF8Text
        baseApi.end()
        return recognizedText;
    }

}