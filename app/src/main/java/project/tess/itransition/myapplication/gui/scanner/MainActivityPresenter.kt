package project.tess.itransition.myapplication.gui.scanner

import android.graphics.Bitmap
import android.net.Uri
import project.tess.itransition.myapplication.utils.scanner.ImageScanner
import project.tess.itransition.myapplication.utils.scanner.ImageScannerListener
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.ImageScanStrategy

/**
 * @author e.fetskovich on 10/19/18.
 */
class MainActivityPresenter(var scanner: ImageScanner) : MainActivityContract.Presenter, ImageScannerListener {

    private var view: MainActivityContract.ViewHelper? = null

    init {
        scanner.scannerListener = this
    }

    override fun onScanStarted() {
        view?.showLoadingDialog()
    }

    override fun onScanFinished(bitmap: Bitmap, recognizedText: String) {
        view?.hideLoadingDialog()
        view?.setLoadedData(bitmap, recognizedText)
    }

    override fun onScanFailed() {
        view?.hideLoadingDialog()
        view?.showErrorMessage()
    }

    override fun pickImage(requestCode: Int) {
        scanner.takePhoto(requestCode)
    }

    override fun captureImage(requestCode: Int) {
        scanner.capturePicture(requestCode)
    }

    override fun onImageCaptured() {
        scanner.onImageTaken()
    }

    override fun onImagePicked(uri: Uri?) {
        if (uri != null) {
            scanner.onImagePicked(uri)
        } else {
            view?.showErrorMessage()
        }
    }

    override fun changeImageScannerType(strategy: ImageScanStrategy) {
        scanner.imageScanStrategy = strategy
    }

    override fun attach(view: MainActivityContract.ViewHelper) {
        this.view = view
    }

    override fun detach() {
        view = null
    }

}