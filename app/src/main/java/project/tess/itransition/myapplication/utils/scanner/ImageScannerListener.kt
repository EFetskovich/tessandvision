package project.tess.itransition.myapplication.utils.scanner

import android.graphics.Bitmap

/**
 * @author e.fetskovich on 10/19/18.
 */
interface ImageScannerListener {
    fun onScanStarted()
    fun onScanFinished(bitmap: Bitmap, recognizedText: String)
    fun onScanFailed()
}
