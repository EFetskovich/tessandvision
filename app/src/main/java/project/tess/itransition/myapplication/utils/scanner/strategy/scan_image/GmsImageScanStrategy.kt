package project.tess.itransition.myapplication.utils.scanner.strategy.scan_image

import android.content.Context
import android.graphics.Bitmap
import project.tess.itransition.myapplication.utils.gmsvisio.GmsVisioScanner

/**
 * @author e.fetskovich on 10/22/18.
 */
class GmsImageScanStrategy(var context: Context) : ImageScanStrategy() {

    override fun scanImage(bitmap: Bitmap): String? {
        return GmsVisioScanner.scanImage(context, bitmap)
    }
}