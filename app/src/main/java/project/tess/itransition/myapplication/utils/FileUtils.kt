package project.tess.itransition.myapplication.utils

import android.os.Environment
import project.tess.itransition.myapplication.TessConfig.TESS_DATA_PATH_PREFIX
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

/**
 * @author e.fetskovich on 10/19/18.
 */
object FileUtils {

    fun getDirectory(folderName: String): String {
        var directory: File? = null
        directory = File(Environment.getExternalStorageDirectory().absolutePath + File.separator + folderName)
        if (!directory.exists()) {
            directory.mkdirs()
        }

        return directory.absolutePath
    }

    fun getTessdataDirectory(directoryPath: String): String {
        val tessdataDirectory = File("$directoryPath/$TESS_DATA_PATH_PREFIX")
        tessdataDirectory.mkdirs()
        return tessdataDirectory.absolutePath
    }

    @Throws(IOException::class)
    fun writeFile(inputStream: InputStream, outputStream: OutputStream) {
        inputStream.use { input ->
            outputStream.use { fileOut ->
                input.copyTo(fileOut)
            }
        }
    }

}
