package project.tess.itransition.myapplication.utils.scanner.strategy.scan_image

import android.graphics.Bitmap
import project.tess.itransition.myapplication.utils.tesseract.TessImageScanner

/**
 * @author e.fetskovich on 10/22/18.
 */
class TessImageScanStrategy(
    private val directoryPath: String,
    private val trainedDataCode: String
) : ImageScanStrategy() {

    override fun scanImage(bitmap: Bitmap): String? {
        return TessImageScanner.scanImage(directoryPath, trainedDataCode, bitmap)
    }

}