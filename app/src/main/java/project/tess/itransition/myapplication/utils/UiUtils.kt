package project.tess.itransition.myapplication.utils

import android.app.ProgressDialog
import android.content.Context
import project.tess.itransition.myapplication.R

/**
 * @author e.fetskovich on 10/19/18.
 */
object UiUtils {

    fun getBaseProgressDialog(context: Context, cancelable: Boolean): ProgressDialog {
        val dialog = ProgressDialog(context)
        dialog.setMessage(context.getString(R.string.progress_dialog_waiting))
        dialog.setCancelable(cancelable)
        return dialog;
    }

}