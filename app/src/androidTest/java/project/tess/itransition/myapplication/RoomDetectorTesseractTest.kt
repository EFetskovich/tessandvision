package project.tess.itransition.myapplication

import android.graphics.BitmapFactory
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import project.tess.itransition.myapplication.gui.scanner.MainActivity
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.TessImageScanStrategy

/**
 * @author e.fetskovich on 10/19/18.
 */

@RunWith(AndroidJUnit4::class)
class RoomDetectorTesseractTest {

    @Test
    fun checkTesseractFunctionality() {
        val appContext = InstrumentationRegistry.getTargetContext()
        val bitmap = BitmapFactory.decodeResource(appContext.resources, R.drawable.door_212)
        val text = TessImageScanStrategy(
            MainActivity.DIRECTORY_PATH,
            MainActivity.TESS_TRAINGED_LANG
        ).scanImageToReceiveItText(bitmap)
        System.out.println(text)
        assert(text.isNullOrEmpty())
    }

}