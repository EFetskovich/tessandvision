package project.tess.itransition.myapplication

import android.graphics.BitmapFactory
import android.support.annotation.DrawableRes
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import project.tess.itransition.myapplication.utils.scanner.strategy.scan_image.GmsImageScanStrategy

/**
 * @author e.fetskovich on 10/22/18.
 */

@RunWith(AndroidJUnit4::class)
class RoomDetectorGmsTest {


    @Test
    fun checkRoom_212() {
        checkRoomText(R.drawable.door_212, "212o")
    }

    @Test
    fun checkRoom_1minus1() {
        checkRoomText(R.drawable.room_1_1, "1-1")
    }

    @Test
    fun checkRoom_1minus2() {
        checkRoomText(R.drawable.room_1_2, "1-2")
    }

    @Test
    fun checkRoom_117a() {
        checkRoomText(R.drawable.room_117_a, "117a")
    }

    @Test
    fun checkRoom_118b() {
        checkRoomText(R.drawable.room_118_b, "118б")
    }

    @Test
    fun checkRoom_119a() {
        checkRoomText(R.drawable.room_119_a, "119a")
    }

    @Test
    fun checkRoom_314b() {
        checkRoomText(R.drawable.room_314_b, "314b")
    }

    @Test
    fun checkRoom_114() {
        checkRoomText(R.drawable.room_114, "114")
    }

    @Test
    fun checkRoom_422ab() {
        checkRoomText(R.drawable.room_422_a_b, "422")
    }

    @Test
    fun checkRoom_208(){
        checkRoomText(R.drawable.room_208, "208")
    }

    @Test
    fun checkRoom_225(){
        checkRoomText(R.drawable.room_225, "225")
    }

    @Test
    fun checkRoom_320(){
        checkRoomText(R.drawable.room_320, "320")
    }

    @Test
    fun checkRoom_313_a(){
        checkRoomText(R.drawable.room_313_a, "313a")
    }

    private fun checkRoomText(@DrawableRes resId: Int, expectedResult: String){
        val appContext = InstrumentationRegistry.getTargetContext()
        val bitmap = BitmapFactory.decodeResource(appContext.resources, resId)
        val text = GmsImageScanStrategy(appContext).scanImageToReceiveItText(bitmap)
        System.out.println("Text: "+text+". Expected: "+expectedResult+". Equals: "+text.equals(expectedResult))
        assertEquals(expectedResult, text)
    }

}